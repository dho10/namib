<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", length=255)
     */
    private $date_scheduled;

    /**
     * @ORM\Column(type="integer", length=20)
     */
    private $estimated_time;

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task): void
    {
        $this->task = $task;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="events")
     */
    private  $task;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDateScheduled(): ?date
    {
        return $this->date_scheduled;
    }

    public function setDateScheduled(string $dateSch) : self
    {
        $this->date_scheduled = $dateSch;
        return $this;
    }

    public function getEstimatedTime(): ?int
    {
        return $this->estimated_time;
    }

    public function setEstimatedTime(string $estimatedTime) : self
    {
        $this->estimated_time = $estimatedTime;
        return $this;
    }


    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->id + "" ;
    }

}
