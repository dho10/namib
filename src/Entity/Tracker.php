<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource
 */
class Tracker
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", length=255)
     */
    private $long;

    /**
     * @ORM\Column(type="float", length=20)
     */
    private $lat;

    /**
     * @ORM\Column(type="float", length=20)
     */
    private $height;

    /**
     * @ORM\Column(type="float", length=20)
     */
    private $temperature;

    /**
     * @ORM\Column(type="float", length=20)
     */
    private $humidity;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Mission", inversedBy="trackers")
     */
    private  $mission ;



    /**
     * @return mixed
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * @param mixed $mission
     */
    public function setMission($mission): void
    {
        $this->mission = $mission;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLong(): ?double
    {
        return $this->long;
    }

    public function setLong(double $long) : self
    {
        $this->long = $long;
        return $this;
    }

    public function getLat(): ?double
    {
        return $this->lat;
    }

    public function setLat(double $lat) : self
    {
        $this->lat = $lat;
        return $this;
    }

    public function getHeight(): ?double
    {
        return $this->height;
    }

    public function setHeight(string $height) : self
    {
        $this->height = $height;
        return $this;
    }

    public function getTemperature(): ?double
    {
        return $this->temperature;
    }

    public function getHumidity(): ?double
    {
        return $this->humidity;
    }


    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->id + "" ;
    }

}
