<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $desc;

    /**
     * @ORM\Column(type="integer", length=20)
     */
    private $freq;

    /**
     * @ORM\Column(type="integer", length=20)
     */
    private $estimatedTime;

    /**
     * @ORM\Column(type="integer", length=20)
     */
    private $ordre;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $task_type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event", mappedBy="task")
     * @ApiSubresource(maxDepth=1)
     */
    private  $events ;



    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param mixed $desc
     */
    public function setDesc($desc): void
    {
        $this->desc = $desc;
    }

    /**
     * @return mixed
     */
    public function getFreq()
    {
        return $this->freq;
    }

    /**
     * @param mixed $freq
     */
    public function setFreq($freq): void
    {
        $this->freq = $freq;
    }

    /**
     * @return mixed
     */
    public function getEstimatedTime()
    {
        return $this->estimatedTime;
    }

    /**
     * @param mixed $estimatedTime
     */
    public function setEstimatedTime($estimatedTime): void
    {
        $this->estimatedTime = $estimatedTime;
    }

    /**
     * @return mixed
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @param mixed $ordre
     */
    public function setOrdre($ordre): void
    {
        $this->ordre = $ordre;
    }

    /**
     * @return mixed
     */
    public function getTaskType()
    {
        return $this->task_type;
    }

    /**
     * @param mixed $task_type
     */
    public function setTaskType($task_type): void
    {
        $this->task_type = $task_type;
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param mixed $events
     */
    public function setEvents($events): void
    {
        $this->events = $events;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->label ;
    }

}
