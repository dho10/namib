<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource
 */
class Mission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $label;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Task")
     *
     * @ApiSubresource(maxDepth=1)
     */
    private  $tasks ;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Equipment")
     * @ApiSubresource(maxDepth=1)
     */
    private  $equipments ;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tracker", mappedBy="mission")
     * @ApiSubresource(maxDepth=1)
     */
    private  $trackers ;


    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->equipments = new ArrayCollection();
        $this->trackers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks): void
    {
        $this->tasks = $tasks;
    }

    /**
     * @return mixed
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * @param mixed $equipments
     */
    public function setEquipments($equipments): void
    {
        $this->equipments = $equipments;
    }

    /**
     * @return mixed
     */
    public function getTrackers()
    {
        return $this->trackers;
    }

    /**
     * @param mixed $trackers
     */
    public function setTrackers($trackers): void
    {
        $this->trackers = $trackers;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->label ;
    }


}
